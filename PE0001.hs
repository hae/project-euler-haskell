-- First problem's solution, proven correct.
-- https://projecteuler.net/problem=1
-- Written by Hector A Escobedo

-- Problem statement: Find the sum of all multiples of 3 or 5 below 1000.

import Data.List (nub)

multiples :: (Num a) => a -> [a]
multiples x = iterate (+ x) x

-- Do the problem's actual calculation. (nub removes duplicate elements
-- from a list.)
result :: Integer
result = (sum . nub) $ (takeWhile (< 1000) (multiples 3)) ++
         (takeWhile (< 1000) (multiples 5))

main = putStrLn (show result)
