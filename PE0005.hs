-- Fifth problem's solution, proven correct.
-- https://projecteuler.net/problem=5
-- Written by Hector A Escobedo

-- Problem statement: Find the smallest positive number evenly divisible by all
-- the integers from 1 to 20.

-- import Data.List (intersect)

-- -- Function that produces an infinite list of a number's increasing multiples.
-- multiples :: (Num a) => a -> [a]
-- multiples x = iterate (+ x) x

-- -- Function the produces the lowest common multiple of two numbers. (intersect
-- -- takes the list intersection of two list, or the elements they have in
-- -- common. It's not cheating.) lcm is already defined in Prelude, so this is
-- -- named lcm' (lcm prime).
-- lcm' :: (Eq a, Num a) => a -> a -> a
-- lcm' x y = head cms
--   where
--     cms = intersect (multiples x) (multiples y)

-- Function that produces the lowest common multiple of a list of numbers.
foldLCM :: (Integral a) => [a] -> a
foldLCM xs = foldr lcm 1 xs

result :: Integer
result = foldLCM [1..20]

main = putStrLn (show result)
