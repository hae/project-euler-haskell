-- Third problem's solution, proven correct and very fast.
-- https://projecteuler.net/problem=3
-- Written by Hector A Escobedo

-- Problem statement: Find the greatest prime factor of 600851475143.

-- Memoized list of prime numbers by nested list comprehension.
-- Could be possibly be replaced with a faster sieve.
primes :: [Integer]
primes = 2 : [ p | p <- [3..],
               and [ p `rem` x > 0 | x <- takeWhile ((<= p) . (^ 2)) primes ] ]

-- Function responsible for narrowing down the list of possible factors.
candidatePrimes :: (Num a, Integral a) => a -> [Integer]
candidatePrimes n = takeWhile (<= ((ceiling . sqrt) (fromIntegral n))) primes

-- Function that produces the prime divisors of a number, except itself.
primeDivisors :: Integer -> [Integer]
primeDivisors n = filter (/= n) $
                  filter ((== 0) . (rem n)) (candidatePrimes n)

-- Function that decides if a number is prime or not.
-- Now optimized so it doesn't always search the whole list of candidates.
isPrime :: Integer -> Bool
isPrime n
  | n < 2 = False
  | otherwise = null (primeDivisors n)

-- Function that produces the prime factors of an integer, least to greatest.
factor :: Integer -> [Integer]
factor 1 = [1] -- Special case, even though 1 is not prime
factor n
  | n < 1 = error "cannot factor non-positive integers"
  | isPrime n = [n]
  | otherwise = leastPD : factor (n `div` leastPD)
  where
    leastPD = head (primeDivisors n)

result :: Integer
result = last (factor 600851475143)

main = putStrLn (show result)
