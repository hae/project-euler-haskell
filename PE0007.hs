-- Seventh problem's solution, proven correct.
-- https://projecteuler.net/problem=7
-- Written by Hector A Escobedo

-- Problem statement: Find the 10001st prime number.

-- Memoized list of prime numbers by nested list comprehension.
-- Could be possibly be replaced with a faster sieve.
primes :: [Integer]
primes = 2 : [ p | p <- [3..],
               and [ p `rem` x > 0 | x <- takeWhile ((<= p) . (^ 2)) primes ] ]

-- Remember that (!!) starts the count at zero
result :: Integer
result = primes !! 10000

main = putStrLn (show result)
