-- Second problem's solution, proven correct.
-- https://projecteuler.net/problem=2
-- Written by Hector A Escobedo

-- Problem statement: Find the sum of all the even-valued Fibonacci numbers less
-- than four million.

fibonaccis :: [Integer]
fibonaccis = 0 : scanl (+) 1 fibonaccis

result :: Integer
result = sum $ filter even $ takeWhile (< 4000000) fibonaccis

main = putStrLn (show result)
