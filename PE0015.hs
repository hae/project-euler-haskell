-- Fifteenth problem solution.
-- https://projecteuler.net/problem=15
-- Written by Hector A Escobedo

-- Problem statement: Find the number of possible paths from the upper leftmost
-- corner to the bottom rightmost corner of a 20 by 20 lattice if the only
-- directions you can move in are down and right.

import Data.Maybe (fromMaybe)

-- type Lattice = (Integer, Integer)

-- -- Produce the total number of vertices in the two-dimensional lattice.
-- vertices :: Lattice -> Integer
-- vertices (x, y) = (x + 1) * (y + 1)

-- -- Produce the total number of edges between vertices in the lattice.
-- edges :: Lattice -> Integer
-- edges (x, y) = ( 2 * x * y) + y + x

-- -- Hopefully a successful implementation of calculating the number of paths
-- -- with a two-dimensional list array.
-- -- This is actually the purely additive form of Pascal's triangle without
-- -- the trivial edges.
-- pathList :: [[Int]]
-- pathList = map ascend [1..]
--   where
--     ascend 1 = [2..]
--     ascend n = (succ n) : zipWith (+) (ascend n) (tail (ascend (pred n)))

-- -- Produce the number of paths.
-- paths :: Lattice -> Int
-- paths (0, 0) = 0
-- paths (0, x) = 1
-- paths (x, 0) = 1
-- paths (x, y) = (pathList !! (x - 1)) !! (y - 1)

-- -- Recursively simulate traversing the paths. Forks, and therefore very slow.
-- -- Still useful for figuring out the algorithm.
-- simulatePaths :: Integer -> Lattice -> Integer
-- simulatePaths count (x, y)
--   | (x == 0) && (y == 0) = 0
--   | x == 0 = 1
--   | y == 0 = 1
--   | otherwise = downPath + rightPath
--   where
--     downPath = simulatePaths (count + 1) (x, y - 1)
--     rightPath = simulatePaths (count + 1) (x - 1, y)

-- Standard factorial definition.
factorial :: (Num a, Integral a) => a -> a
factorial n = product [1..n]

-- Challenge for myself, for later: create the general binomial coefficient
-- function to recreate Pascal's triangle.

-- This should be used like 10 `choose` 5, for calculating a binomial
-- coefficient, so it looks like a natural language expression.
combinations :: (Num a, Integral a) => a -> a -> a
combinations n k = (factorial n) `div` ((factorial k) * (factorial (n - k)))

-- Sometimes synonyms still need type annotations, apparently.
choose :: (Num a, Integral a) => a -> a -> a
choose = combinations

-- Pascal's triangle indexed by row.
pascalsTriangle :: (Num a, Integral a) => [[a]]
pascalsTriangle = map (\ x -> map (choose x) [0..x]) [0..]

-- Helper function that returns the middle element of a list, using some
-- pretty sweet pattern matching, if I say so myself.
mid :: [a] -> Maybe a
mid [] = Nothing
mid [x] = Just x
mid (x:xs) = mid . init $ xs

-- Thank you very much, Google and Wikipedia.
centralBinomialCoefficient :: (Num a, Integral a) => a -> a
centralBinomialCoefficient x = fromMaybe 0 $ -- Never happens.
                               mid (pascalsTriangle !! (fromIntegral (2 * x)))

result :: Integer
result = centralBinomialCoefficient 20

main = putStrLn (show result)
