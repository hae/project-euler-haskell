-- Sixth problem's solution, proven correct.
-- https://projecteuler.net/problem=6
-- Written by Hector A Escobedo

-- Problem statement: Find the difference between the sum of the squares of the
-- first one hundred natural numbers and the square of the sum.

square :: (Num a) => a -> a
square = (^ 2)

sumSquares :: (Num a) => [a] -> a
sumSquares = sum . (map square)

squareSum :: (Num a) => [a] -> a
squareSum = square . sum

result :: Integer
result = (squareSum ls) - (sumSquares ls)
  where
    ls = [1..100]

main = putStrLn (show result)
