-- Fourth problem's solution.
-- https://projecteuler.net/problem=4
-- Written by Hector A Escobedo

-- Problem statement: Find the largest palindromic number that is the product
-- of two three-digit numbers.

digits :: Integer -> [Integer]
digits n
  | n < 0 = error "Negative numbers unsupported"
  | n >= 0 = map (\ d -> read [d] :: Integer) (show n)

-- Construct an infinite list of palindromic numbers
palindromes :: [Integer]
palindromes = undefined
